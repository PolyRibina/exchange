<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $fillable = ['date', 'price', 'qty', 'status', 'prev_state', 'side', 'instrument', 'trader', 'broker'];

    public function setStatus($value)
    {
        $this->attributes['status'] = strtolower($value);
    }

    public function setTrader($value)
    {
        $this->attributes['trader'] = strtolower($value);
    }

    public function setSide($value)
    {
        $this->attributes['side'] = strtolower($value);
    }
}
