<?php

namespace App\Http\Controllers;

use App\Bid;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Echo_;

class BidController extends Controller
{
    public function index()
    {
        return Bid::all();
    }

    public function show(Bid $product)
    {
        return $product;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'instrument' => 'required',
            'price' => 'required',
            'qty' => 'required',
        ]);
        $request->input('status', 1);
        $product = Bid::create($request->all());

        return response()->json($product, 201);
    }


    public function update(Request $request, Bid $product)
    {
        $product->update($request->all());

        return response()->json($product, 200);
    }

    public function delete(Bid $product)
    {
        $product->delete();

        return response()->json(null, 204);
    }
}
