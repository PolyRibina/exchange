<?php

namespace App\Http\Controllers;

use App\Trader;
use Illuminate\Http\Request;

class TraderController extends Controller
{
    public function index()
    {
        return Trader::all();
    }

    public function show(Trader $trader)
    {
        return $trader;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:products|max:255',
            'description' => 'required',
            'price' => 'integer',
            'availability' => 'boolean',
        ]);
        $product = Trader::create($request->all());

        return response()->json($product, 201);
    }

    public function update(Request $request, Trader $trader)
    {
        $trader->update($request->all());

        return response()->json($trader, 200);
    }

    public function delete(Trader $trader)
    {
        $trader->delete();

        return response()->json(null, 204);
    }
}
