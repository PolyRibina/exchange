<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market_table extends Model
{
    protected $fillable = ['title', 'description', 'price', 'availability'];

}
