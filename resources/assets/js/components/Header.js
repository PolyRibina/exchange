import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Grid} from "@material-ui/core";

function ButtonAppBar() {

    return (
        <div>
            <AppBar position="static" color="inherit">
                <Toolbar>
                    <Typography variant="h3" gutterBottom>
                        EXchan$e
                    </Typography>
                    <Grid container spacing={3}>
                        <Grid item xs>
                            <Button size="large">Главная</Button>
                        </Grid>
                        <Grid item xs>
                            <Button size="large">Котировки</Button>
                        </Grid>
                        <Grid item xs>
                            <Button size="large" disabled>Графики</Button>
                        </Grid>
                    </Grid>

                    <Button size="large">Войти</Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default class Header extends Component {
    render() {
        return (
            <div className="container">
                <ButtonAppBar/>
            </div>
        );
    }
}

if (document.getElementById('header')) {
    ReactDOM.render(<Header />, document.getElementById('header'));
}
