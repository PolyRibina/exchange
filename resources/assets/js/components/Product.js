import React, { Component } from 'react';

/* Stateless component or pure component
 * { product } syntax is the object destructing
 */
const Trader = ({trader}) => {
   
  const divStyle = {
      display: 'flex',
      flexDirection: 'column',
      width: '65%',
      margin: '30px 10px 10px 30px'
  }

  //if the props for product is null, return Product doesn't exist
  if(!trader) {

    return(<div style={divStyle}><h2>  No Product was selected </h2> </div>);
  }
    
  //Else, display the product data
  return(  
    <div style={divStyle}> 
      <h2> {trader.name} </h2>
      <p> {trader.email} </p>
      <h3> Status {trader.availability ? 'Available' : 'Out of stock'} </h3>
      <h3> Price : {trader.password} </h3>
     
    </div>
  )
}

export default Trader ;