<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/quotation', function () {
    return view('quotation');
});
Route::get('/graph', function () {
    return view('graph');
});
Route::get('/auth', function () {
    return view('auth');
});
Route::get('/check-in', function () {
    return view('checkin');
});
